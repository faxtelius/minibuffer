#include <iostream>
#include "minibuffer.hpp"

#pragma GCC diagnostic ignored "-Wunused-parameter"

const int X = 32;
const int Y = 8;

unsigned char *buffer = NULL;

int main(int argc, char *argv[])
{
	buffer = new unsigned char[X * Y];

	minibuffer b = minibuffer();

	for (int i = 0; i < X * Y; i++)
	{
		buffer[i] = 0; // Initialize all elements to zero.
	}

	buffer[10] = 1;

	if (buffer[0] == 0)
	{
		std::cout << "0 == 0\n";
	}

	if (buffer[10] == 1)
	{
		std::cout << "10 == 1\n";
	}

	std::cout << "Hello Easy C++ project!!" << std::endl;

	delete[] buffer;
	buffer = NULL;
}
